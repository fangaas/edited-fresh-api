var express = require('express');
var app = express();

app.use(express.bodyParser());

var request = require('request');
var http = require('http');
var fs = require('fs');

var access_token = null;

function readAccessToken() {
	fs.readFile('access_token.txt', 'utf8', function (err,data) {
		if (err) {
			log("Access token file doesn't exist");
			return;
		}
		log("Access token loaded");
		console.log(data);
		access_token = data;
	});
}

function writeAccessToken(token) {
	fs.writeFile('access_token.txt', token, function (err) {
		if (err) {
			log("Couldn't write to access token file");
			return; 
		}
		log("Access token updated");
	});
}

console.log("API Started, waiting for calls...");

function log(message) {
	console.log("--" + message + "--");
}

//Should not need to recall this since we have the access token
function getAccessToken(requestCallbackObject, responseObject, attempts) {
	log("Getting new access token");

	request({
	    url: 'https://api.molt.in/oauth/access_token', //URL to hit
	    method: 'POST', //Specify the method
	    form: { //We can define headers too
	        'grant_type': 'client_credentials',
	        'client_id': '3fTqEGayHJKh2Gu0fNCypkE2DABja3A1WRa610o8',
	        'client_secret': 'Ca66goOpl8BjvqJ71MPHYSdpFvReYQiGz7GyHsp8'
	    }  
	}, function(error, response, body){
	    if(error) {
	        console.log(error);
	    } else {
	        var access_token = JSON.parse(body).access_token;
	        writeAccessToken(access_token);
	        requestCallbackObject.auth.bearer = access_token;
	        retryRequest(requestCallbackObject, responseObject, attempts);
	    }
	});
}

function refreshAccessToken(requestCallbackObject, responseObject, attempts) {
	log("Refreshing access token");

	request({
	    url: 'https://api.molt.in/oauth/access_token', //URL to hit
	    method: 'POST', //Specify the method
	    form: { //We can define headers too
	        'grant_type': 'refresh_token',
	        'client_id': '3fTqEGayHJKh2Gu0fNCypkE2DABja3A1WRa610o8',
	        'client_secret': 'Ca66goOpl8BjvqJ71MPHYSdpFvReYQiGz7GyHsp8',
	        'refresh_token': access_token
	    }  
	}, function(error, response, body){
	    var bodyResp = JSON.parse(body);
	    if(bodyResp.error) {
	    	log("Could not refresh access token");
			getAccessToken(requestCallbackObject, responseObject, attempts);
	    } else {
	        retryRequest(requestCallbackObject, responseObject, attempts);
	    }
	});	
}

function makeRequest(url, postorget, dataObject, responseObject, attempts) {
	log("Making request, URL: " + url);

	readAccessToken();

	var requestObject = {
	    url: url,
	    method: postorget, //Specify the method
	    auth: {
	    	'bearer': access_token
	    },
	    form: dataObject  
	};

	request(requestObject, function(error, response, body){
		var bodyResp = JSON.parse(body);

		//if(bodyResp.error == "Unable to validate access token") {
		if(bodyResp.error) {
			log("Current access token possibly invalid");
			refreshAccessToken(requestObject, responseObject, attempts);
		} else {
	    	successCallback(responseObject, bodyResp);
		}

	});
}

function retryRequest(requestCallbackObject, responseObject, attempts) {
	log("Retrying request");

	attempts++;

	request(requestCallbackObject, function(error, response, body){
		var bodyResp = JSON.parse(body);

		if(bodyResp.error) {
			if(attempts < 3) {
				getAccessToken(requestCallbackObject, responseObject, attempts);
			} else {
				failureCallback(responseObject, bodyResp);
			}
		} else {
	    	successCallback(responseObject, bodyResp);
		}

	});

}

function successCallback(responseObject, body) {

	log("Success");
	console.log(body);

	responseObject.type('application/json');
	responseObject.send(JSON.stringify(body));

	log("Request over");

}

function failureCallback(responseObject, body) {

	log("Failure");
	console.log(body);

	responseObject.type('application/json');
	responseObject.send(JSON.stringify(body));

	log("Request over");

}


function addCustomer(responseObject, customerData) {

	/*var customerData = { 
    	first_name: "John",
    	last_name: "Doe",
    	email: "anotheremeail3@email.com",
    	password: "pass"
    },*/

    var url = 'https://api.molt.in/v1/customers';

	makeRequest(url, 'POST', customerData, responseObject, 1);

}

function loginCustomer(responseObject, loginData) {

	/*var loginData = {
		email: "email@email.com",
		password: "pass"
	}*/

	var url = 'https://api.molt.in/v1/customers/authenticate';

	makeRequest(url, 'POST', loginData, responseObject, 1);

}


app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

//refreshAccessToken();

//addCustomer();
app.all('/', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	next();
});


app.get('/', function(req, res, next) {

	res.type('text/plain');
	res.send('i am a beautiful butterfly');

});

app.post('/addCustomer', function(req, res) {

	addCustomer(res, req.body);

});

app.post('/loginCustomer', function(req, res) {

	loginCustomer(res, req.body);

});

app.listen(process.env.PORT || 4730);