//Sample successful payment
{
    "status": true,
    "result": {
        "message": "Success",
        "redirect": false,
        "reference": "56f91baf66ee5",
        "data": {
            "amount": "33.48",
            "reference": "56f91baf66ee5",
            "success": true,
            "message": "Success"
        },
        "order": {
            "id": "1215895043005481161",
            "order": null,
            "created_at": "2016-03-28 11:53:25",
            "updated_at": "2016-03-28 11:53:25",
            "customer": {
                "value": "Bob",
                "data": {
                    "id": "1212267077579047478",
                    "order": null,
                    "created_at": "2016-03-23 11:45:17",
                    "updated_at": "2016-03-23 11:45:17",
                    "first_name": "Bob",
                    "last_name": "Doe",
                    "email": "anotheremeail5@email.com",
                    "group": null,
                    "password": true
                }
            },
            "gateway": {
                "value": "Dummy",
                "data": {
                    "name": "Dummy",
                    "slug": "dummy",
                    "description": null,
                    "enabled": true
                }
            },
            "status": {
                "value": "Paid",
                "data": {
                    "key": "paid",
                    "value": "Paid"
                }
            },
            "subtotal": 22.95,
            "shipping_price": 5.94,
            "total": 33.48,
            "currency": {
                "value": "British Pound",
                "data": {
                    "id": "1176187812622369728",
                    "code": "GBP",
                    "title": "British Pound",
                    "enabled": true,
                    "modifier": "+0",
                    "exchange_rate": 0,
                    "format": "£{price}",
                    "decimal_point": ".",
                    "thousand_point": ",",
                    "rounding": null,
                    "default": true,
                    "created_at": null,
                    "updated_at": null
                }
            },
            "currency_code": "GBP",
            "exchange_rate": 0,
            "shipping": {
                "value": "Next Day Delivery",
                "data": {
                    "id": "1200661103210659987",
                    "order": null,
                    "created_at": "2016-03-07 11:26:18",
                    "updated_at": "2016-03-16 14:44:11",
                    "title": "Next Day Delivery",
                    "slug": "next-day",
                    "company": "Courier",
                    "status": {
                        "value": "Live",
                        "data": {
                            "key": "1",
                            "value": "Live"
                        }
                    },
                    "price_min": 0,
                    "price_max": 0,
                    "weight_min": 0,
                    "weight_max": 0,
                    "tax_band": {
                        "value": "Default",
                        "data": {
                            "id": "1176187812689478512",
                            "title": "Default",
                            "description": null,
                            "rate": 20,
                            "created_at": null,
                            "updated_at": null
                        }
                    },
                    "description": "",
                    "price": {
                        "value": "£5.94",
                        "data": {
                            "formatted": {
                                "with_tax": "£5.94",
                                "without_tax": "£4.95",
                                "tax": "£0.99"
                            },
                            "rounded": {
                                "with_tax": 5.94,
                                "without_tax": 4.95,
                                "tax": 0.99
                            },
                            "raw": {
                                "with_tax": 5.94,
                                "without_tax": 4.95,
                                "tax": 0.99
                            }
                        }
                    }
                }
            },
            "ship_to": {
                "value": "",
                "data": {
                    "id": "1215864890867056819",
                    "order": null,
                    "created_at": "2016-03-28 10:53:30",
                    "updated_at": "2016-03-28 10:53:30",
                    "save_as": "",
                    "first_name": "Mat",
                    "last_name": "J",
                    "address_1": "16 Southampton Street",
                    "address_2": "",
                    "postcode": "BN2 9UT",
                    "country": {
                        "value": "United Kingdom",
                        "data": {
                            "code": "GB",
                            "name": "United Kingdom"
                        }
                    },
                    "company": "",
                    "city": "Brighton",
                    "customer": {
                        "value": "Bob",
                        "data": {
                            "id": "1212267077579047478",
                            "order": null,
                            "created_at": "2016-03-23 11:45:17",
                            "updated_at": "2016-03-23 11:45:17",
                            "first_name": "Bob",
                            "last_name": "Doe",
                            "email": "anotheremeail5@email.com",
                            "group": null,
                            "password": true
                        }
                    },
                    "phone": "7963759142",
                    "county": "East Sussex",
                    "instructions": ""
                }
            },
            "bill_to": {
                "value": "",
                "data": {
                    "id": "1215864890867056819",
                    "order": null,
                    "created_at": "2016-03-28 10:53:30",
                    "updated_at": "2016-03-28 10:53:30",
                    "save_as": "",
                    "first_name": "Mat",
                    "last_name": "J",
                    "address_1": "16 Southampton Street",
                    "address_2": "",
                    "postcode": "BN2 9UT",
                    "country": {
                        "value": "United Kingdom",
                        "data": {
                            "code": "GB",
                            "name": "United Kingdom"
                        }
                    },
                    "company": "",
                    "city": "Brighton",
                    "customer": {
                        "value": "Bob",
                        "data": {
                            "id": "1212267077579047478",
                            "order": null,
                            "created_at": "2016-03-23 11:45:17",
                            "updated_at": "2016-03-23 11:45:17",
                            "first_name": "Bob",
                            "last_name": "Doe",
                            "email": "anotheremeail5@email.com",
                            "group": null,
                            "password": true
                        }
                    },
                    "phone": "7963759142",
                    "county": "East Sussex",
                    "instructions": ""
                }
            }
        }
    }
}