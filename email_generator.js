/*returns
var returnObj = {
	subject:
	html:
	textOnly:
}
*/

var handlebars = require('handlebars'),
http = require('http'),
fs = require('fs'),
nodemailer = require('nodemailer');

var niftySmtpConfig = {
	host: "smtp.edited.co.uk",
	auth: {
		user: "mailer@edited.co.uk",
		pass: "wJm604006!!"
	},
	tls: {
		rejectUnauthorized: false
	} 
}

var editedReceiveEmail = "fangaas@gmail.com;hello@edited.co.uk";
var internalEmail = "fangaas@gmail.com";

var editedSendEmail = 'Edited Webstore <no.reply@edited.co.uk>';


function log(message) {
	console.log("Email generator says: " + message);
}

function generateAndSendMail(emailSpec) {

	/*emailSpec = {
		type:
		textOnly:  
		firstName:
		email:
		generatedPassword:
		message: 
	}*/
	switch(emailSpec.type) {
		case "feedbackEmail":
			loadTemplate("base_email.html", emailSpec, feedbackReceivedEmail);
		break;
		case "passwordReset":
			loadTemplate("base_email.html", emailSpec, passwordResetEmail);
		break;
		case "newCustomer":
			loadTemplate("base_email.html", emailSpec, newCustomerEmail);
		break;
		case "newCustomerGenPass":
			loadTemplate("base_email.html", emailSpec, newCustomerEmailGenPass);
		break;
		case "changePassword":
			loadTemplate("base_email.html", emailSpec, changePasswordEmail);
		break;
		case "sendContactFormEmail":
			sendContactFormEmail(emailSpec);
		break;
	}

}

function generateAndSendReceipt(emailSpec) {

	/*emailSpec = {
		type: customer/internal 
		email:
		receiptSummaryJson:
		receiptItemsArray:
	}*/

	loadTemplate("receipt_email.html", emailSpec, receiptEmail);

}

function passwordResetEmail(html, emailSpec) {
	
	var template = handlebars.compile(html);

	var subject = "Password reset request received";

	var context = {
		title: "Password reset request",
		firstName: emailSpec.firstName,
		mainMessage: "This is an email to let you know that you (or someone else) has requested to reset the password on your account, please use the below password to log into your account, you can then change your password via account management. If you don't think you should have received this email, please contact us.",
		genPass: emailSpec.generatedPassword,
		genPassDisplay: "block",
		visitSiteDisplay: "block"
	};

	var evaluatedHtml = template(context);

	var textOnly = "Hi " + emailSpec.firstName + ", This is an email to let you know that you (or someone else) has requested to reset the password on your account, please use generated password " + emailSpec.generatedPassword + " to log into your account, you can then change your password via account management. If you don't think you should have received this email, please contact us.";

	var returnObj = {
		html: evaluatedHtml,
		textOnly: textOnly,
		subject: subject,
		sendTo: emailSpec.email
	}

	deployEmail(returnObj);

}

function feedbackReceivedEmail(html, emailSpec) {

	var template = handlebars.compile(html);

	var subject = "Feedback received at checkout";

	var context = {
		title: "Feedback received at checkout",
		firstName: "Edited People",
		mainMessage: '"' + emailSpec.message + '"',
		genPassDisplay: "none",
		visitSiteDisplay: "none"
	};

	var evaluatedHtml = template(context);

	var textOnly = "Someone left some feedback right before they left the checkout process, hopefully it's not too harsh...<br><br> '" + emailSpec.message + "'";

	var returnObj = {
		html: evaluatedHtml,
		textOnly: textOnly,
		subject: subject,
		sendTo: internalEmail
	}

	deployEmail(returnObj);

}

function newCustomerEmail(html, emailSpec) {

	var template = handlebars.compile(html);

	var subject = "New Account Created for Edited Online";

	var context = {
		title: "New account created",
		firstName: emailSpec.first_name,
		mainMessage: 'Thanks for signing up! Now that you are signed up to Edited Online you can be sure to get the best experience and greatest ease-of-use out of our online store. Sign in to your account using your email address and chosen password.',
		genPassDisplay: "none",
		visitSiteDisplay: "none"
	};

	var evaluatedHtml = template(context);

	var textOnly = 'Thanks for signing up! Now that you are signed up to Edited Online you can be sure to get the best experience and greatest ease-of-use out of our online store. Sign in to your account using your email address and chosen password.';

	var returnObj = {
		html: evaluatedHtml,
		textOnly: textOnly,
		subject: subject,
		sendTo: emailSpec.email
	}

	deployEmail(returnObj);

}

function newCustomerEmailGenPass(html, emailSpec) {

	var template = handlebars.compile(html);

	var subject = "New Account Created for Edited Online";

	var context = {
		title: "New account created",
		firstName: emailSpec.first_name,
		mainMessage: 'Now that you are signed up to Edited Online you can be sure to get the best experience and greatest ease-of-use out of our online store. Sign in to your account using your email address and the generated password below.',
		genPass: emailSpec.generatedPassword,
		genPassDisplay: "block",
		visitSiteDisplay: "block"
	};

	var evaluatedHtml = template(context);

	var textOnly = 'Now that you are signed up to Edited Online you can be sure to get the best experience and greatest ease-of-use out of our online store. Sign in to your account using your email address and generated password: ' + emailSpec.generatedPassword;

	var returnObj = {
		html: evaluatedHtml,
		textOnly: textOnly,
		subject: subject,
		sendTo: emailSpec.email
	}

	deployEmail(returnObj);

}

function changePasswordEmail(html, emailSpec) {

	var template = handlebars.compile(html);

	var subject = "Edited Online Password Changed";

	var context = {
		title: "Password changed",
		firstName: emailSpec.first_name,
		mainMessage: "Your password has been changed against your Edited account, if this wasn't you then please contact support@edited.co.uk and we can help you out.",
		genPassDisplay: "none",
		visitSiteDisplay: "none"
	};

	var evaluatedHtml = template(context);

	var textOnly = "Your password has been changed against your Edited account, if this wasn't you then please contact support@edited.co.uk and we can help you out.";

	var returnObj = {
		html: evaluatedHtml,
		textOnly: textOnly,
		subject: subject,
		sendTo: emailSpec.email
	}

	deployEmail(returnObj);

}

function receiptEmail(html, emailSpec) {

	var template = handlebars.compile(html);

	/*emailSpec = {
		type: customer/internal 
		email:
		receiptSummaryJson:
		receiptItemsArray:
	}*/

	var subject,
	receiptSummaryJson = emailSpec.receiptSummaryJson,
	receiptItemsArray = emailSpec.receiptItemsArray;

	var context = {
		title: "",
		message: "",
		receiptSummaryJson: emailSpec.receiptSummaryJson,
		receiptItemsArray: emailSpec.receiptItemsArray
	}

	var orderId = receiptSummaryJson.id;

	var customerOrderId = "WO-" + orderId.substr(orderId.length - 4, orderId.length);

	if(emailSpec.type == "customer") {
		subject = "Edited Online Store Receipt | Order: " + customerOrderId;
		context.title = "Receipt";
	} else if(emailSpec.type == "internal") {
		subject = "New Online Store Order | Order: " + customerOrderId + " | " + receiptSummaryJson.id;
		context.title = "New Order";
		context.message = "Customer name: " + receiptSummaryJson.customer.data.first_name + " " + receiptSummaryJson.customer.data.last_name + "<br>Customer email: " + receiptSummaryJson.customer.data.email + "<br>Chosen delivery method: " + receiptSummaryJson.shipping.value;
		emailSpec.email = editedReceiveEmail;
	}

	var evaluatedHtml = template(context);

	var textOnly = "Edited receipt (text only), to view your receipt please visit www.edited.co.uk/receipt" + receiptSummaryJson.id;

	var returnObj = {
		html: evaluatedHtml,
		textOnly: textOnly,
		subject: subject,
		sendTo: emailSpec.email
	}

	deployEmail(returnObj);

}


function loadTemplate(templateName, emailSpec, callback) {

	fs.readFile('email_templates/' + templateName, "utf-8", function (err, html) {
	    callback(html, emailSpec);
	});

}

function deployEmail(emailData) {

	/*emailData = {
		html: evaluatedHtml,
		textOnly: textOnly,
		subject: subject,
		sendTo: emailSpec.email
	}*/

	var transporter = nodemailer.createTransport(niftySmtpConfig);

	var mailOptions = {
	    from: editedSendEmail, // sender address
	    to: emailData.sendTo, // list of receivers
	    subject: emailData.subject, // Subject line
	    text: emailData.textOnly, // plaintext body
	    html: emailData.html // html body
	};

	transporter.sendMail(mailOptions, function(error, info){

	    if(error){
	     	log("ERROR when sending message: " + error);
	    } else {
	     	log("Message sent successfully");
	    }
	    
	});

}

function sendContactFormEmail(emailData) {

	/*emailData = {
		name:
		email:
		message:
	}*/

	var newEmailData = {
		subject: "New Enquiry From The Webstore",
		sendTo: editedReceiveEmail
	}

	var html = "<b>From:</b> " + emailData.name + "<br><br>";
	html += "<b>Email address:</b> " + emailData.email + "<br><br>";
	html += "<b>Message:</b> " + emailData.message;

	newEmailData.html = html;
	newEmailData.textOnly = html;

	deployEmail(newEmailData);

	log("Sending web store customer enquiry");

}


/*var emailSpec = {
	type: "passwordReset",
	firstName: "Jonby",
	email: "jonbyjones@gmail.com",
	generatedPassword: "sdasds!as"
};

generateAndSendMail(emailSpec);*/


module.exports = {
	generateAndSendMail: generateAndSendMail,
	generateAndSendReceipt: generateAndSendReceipt
};