var express = require('express');
var app = express();

app.use(express.bodyParser());

var nodemailer = require('nodemailer');

var niftySmtpConfig = {
	host: "smtp.niftyretail.com",
	auth: {
		user: "mailer@niftyretail.com",
		pass: "wJm604006!!"
	},
	tls: {
		rejectUnauthorized: false
	} 
}

var request = require('request');
var http = require('http');
var fs = require('fs');

app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

app.all('/', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	next();
});


app.get('/', function(req, res, next) {

	res.type('text/plain');
	res.send('This is our mini API');

});

app.post('/sendNiftyEmail', function(req, res) {

	try {
		sendNiftyEmail(res, req.body);
	} catch(error) {
		var responseData = {
    		success: false
    	};
    	responseObject.type('application/json');
		responseObject.send(JSON.stringify(responseData));
	}

});

app.listen(process.env.PORT || 4730);

function apiLog(message) {
	var logDate = new Date();
	console.log(logDate.toLocaleTimeString() + "---" + message + "---");
}

function apiWarn() {
	console.log("-NEVER close this window-");	
}

function sendNiftyEmail(responseObject, bodyData) {

	apiLog("Sending contact message via API " + JSON.stringify(bodyData));

	var transporter = nodemailer.createTransport(niftySmtpConfig);

	var subject = "Nifty Retail Website Contact";

	var message = "<b>Message from: </b>" + bodyData.name + "<br><br>" 
					+ "<b>Email: </b>" + bodyData.email + "<br><br>"
					+ "<b>Message: </b>" + bodyData.message;

	var sendTo = 'contact@niftyretail.com';

	if(bodyData.sendTo !== undefined) sendTo = bodyData.sendTo;

	var mailOptions = {
	    from: bodyData.name + ' <mailer@niftyretail.com>', // sender address
	    to: sendTo, // list of receivers
	    subject: subject, // Subject line
	    text: message, // plaintext body
	    html: message // html body
	};

	transporter.sendMail(mailOptions, function(error, info){
    	var responseData = {
    		success: true
    	};

	    if(error){
	     	responseData.success = false;   
	     	apiLog("ERROR when sending message: " + error);
	    } else {
	     	apiLog("Message sent successfully");
	    }

	    apiWarn();

	    responseObject.type('application/json');
		responseObject.send(JSON.stringify(responseData));
	    
	});

}

apiLog("API Started");
apiWarn();