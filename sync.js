var express = require('express');
var app = express();

app.use(express.bodyParser());

var request = require('request');
var http = require('http');
var fs = require('fs');
var xmlParser = require('xml2js').parseString;
var schedule = require('node-schedule');
var mysql = require('mysql');

var access_token = null;
var assembledResponseData;
var dbConnection;

String.prototype.replaceAll = function(str1, str2, ignore) 
{
	return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
}

function readAccessToken() {
	fs.readFile('access_token.txt', 'utf8', function (err,data) {
		if (err) {
			log("Access token file doesn't exist");
			return;
		}
		log("Access token loaded");
		console.log(data);
		access_token = data;
	});
}

function writeAccessToken(token) {
	fs.writeFile('access_token.txt', token, function (err) {
		if (err) {
			log("Couldn't write to access token file");
			return; 
		}
		log("Access token updated");
	});
}

readAccessToken();

console.log("Sync service started...");

function log(message) {
	console.log("-- Request log: " + message + "--");
}

//Should not need to recall this since we have the access token
function getAccessToken(requestCallbackObject, attempts, requestCallback, requestFailureCallback, noRetry) {
	log("Getting access token");

	request({
	    url: 'https://api.molt.in/oauth/access_token', //URL to hit
	    method: 'POST', //Specify the method
	    form: { //We can define headers too
	        'grant_type': 'client_credentials',
	        'client_id': '3fTqEGayHJKh2Gu0fNCypkE2DABja3A1WRa610o8',
	        'client_secret': 'Ca66goOpl8BjvqJ71MPHYSdpFvReYQiGz7GyHsp8'
	    }  
	}, function(error, response, body){
	    if(error) {
	        console.log(error);
	    } else {
	        access_token = JSON.parse(body).access_token;
	        writeAccessToken(access_token);
	        requestCallbackObject.auth.bearer = access_token;
	        retryMoltinRequest(requestCallbackObject, attempts, requestCallback, requestFailureCallback, noRetry);
	    }
	});
}

function refreshAccessToken(requestCallbackObject, attempts, requestCallback, requestFailureCallback, noRetry) {
	log("Refreshing access token");

	request({
	    url: 'https://api.molt.in/oauth/access_token', //URL to hit
	    method: 'POST', //Specify the method
	    form: { //We can define headers too
	        'grant_type': 'refresh_token',
	        'client_id': '3fTqEGayHJKh2Gu0fNCypkE2DABja3A1WRa610o8',
	        'client_secret': 'Ca66goOpl8BjvqJ71MPHYSdpFvReYQiGz7GyHsp8',
	        'refresh_token': access_token
	    }  
	}, function(error, response, body){
	    var bodyResp = JSON.parse(body);
	    if(bodyResp.error) {
			getAccessToken(requestCallbackObject, attempts, requestCallback, requestFailureCallback, noRetry);
	    } else {
	        retryMoltinRequest(requestCallbackObject, attempts, requestCallback, requestFailureCallback, noRetry);
	    }
	});	
}

function makeMoltinRequest(url, postorget, dataObject, attempts, requestCallback, requestFailureCallback, noRetry) {
	//log("Making request, URL: " + url);

	var requestObject = {
	    url: url,
	    method: postorget, //Specify the method
	    auth: {
	    	'bearer': access_token
	    }
	};

	if(dataObject) {
		requestObject["form"] = dataObject;
	}

	assembledResponseData = [];

	request(requestObject, function(error, response, body){
		var bodyResp = JSON.parse(body);

		//if(bodyResp.error == "Unable to validate access token") {
		if(bodyResp.error) {
			refreshAccessToken(requestObject, attempts, requestCallback, requestFailureCallback, noRetry);
		} else if(bodyResp.pagination && bodyResp.pagination.links.next && !noRetry) {
			log("Data received, additional data present, moving to next page");
			assembledResponseData =  assembledResponseData.concat(bodyResp.result);
			requestObject.url = bodyResp.pagination.links.next;
			retryMoltinRequest(requestObject, attempts, requestCallback, requestFailureCallback, noRetry);
		} else {
			assembledResponseData = assembledResponseData.concat(bodyResp.result);
	    	requestCallback(assembledResponseData);
		}

	});
}

function retryMoltinRequest(requestObject, attempts, requestCallback, requestFailureCallback, noRetry) {
	log("Repeating/retrying request");


	request(requestObject, function(error, response, body){
		var bodyResp = JSON.parse(body);

		if(bodyResp.error) {
			attempts++;

			if(attempts < 3) {
				getAccessToken(requestObject, attempts, requestCallback, requestFailureCallback, noRetry);
			} else {
				requestFailureCallback(error);
			}
		} else if(bodyResp.pagination && bodyResp.pagination.links.next && !noRetry) {
			log("Data received, additional data present, moving to next page");
			assembledResponseData = assembledResponseData.concat(bodyResp.result);
			requestObject.url = bodyResp.pagination.links.next;
			retryMoltinRequest(requestObject, attempts, requestCallback, requestFailureCallback, noRetry);
		} else {
			assembledResponseData = assembledResponseData.concat(bodyResp.result);
	    	requestCallback(assembledResponseData);
		}

	});

}

function successCallback(responseObject, body) {

	log("Success");
	console.log(body);

	responseObject.type('application/json');
	responseObject.send(JSON.stringify(body));

	log("Request over");

}

function failureCallback(responseObject, body) {

	log("Failure");
	console.log(body);

	responseObject.type('application/json');
	responseObject.send(JSON.stringify(body));

	log("Request over");

}



function runSync() {

	function syncLog(message) {
		var logDate = new Date();
		var localMessage = logDate.toLocaleTimeString() + "---" + message + "---";
		var message = logDate.toLocaleDateString() + " " + logDate.toLocaleTimeString() + "---" + message + "--- \r\n";
		console.log(localMessage);

		fs.appendFile('sync_log.txt', message, function (err) {
			if (err) {
				//log("Couldn't write to log file");
				return; 
			}
			//log("Sync log updated");
		});
	}

	var googleItems,
	googleBrands = [],
	googleCategories = [],
	googleTaxes = [],
	googleProducts = [],
	googleProductParseFailures = [],
	moltinBrands,
	moltinProducts,
	moltinCategories,
	moltinTaxes,
	updateObjects = [],
	productUpdateArray = [],
	dbRows = [],
	retryCount = 0;

	syncLog("Beginning new sync process");

	//First get the full XML from the google product feed
	function getGoogleXML() {

		syncLog("Getting XML from Google");

		var requestObject = {
		    url: "http://www.data.edited.co.uk/googlemerchant.xml",
		    method: "GET"  
		};

		request(requestObject, function(error, response, body){
			if(error) {
				handleSyncError("XML not received: " + error);
			} else {
				syncLog("XML received successfully");

				parseGoogleXML(body);

			}
		});

		/*fs.readFile('googlemerchant.xml', 'utf8', function (err,data) {
			if (err) {
				return;
			}
			parseGoogleXML(data);
		});*/

	}

	//Parse into something readable
	function parseGoogleXML(bodyXml) {

		syncLog("Parsing XML to JSON");

		xmlParser(bodyXml, function(error, result) {
			if(error) {
				handleSyncError(error);
			} else {
				syncLog("XML parsed successfully");

				googleItems = result.rss.channel[0].item;
				walkJsonData();
			}
		});
	}

	function walkJsonData() {

		for(var i = 0; i < googleItems.length; i ++) {

			var brand,
			category,
			cur = googleItems[i];

			//Get list of brands from Google xml
			try {
				brand = cur["g:brand"][0];
				if(googleBrands.indexOf(brand) == -1) {
					googleBrands.push(brand);
				}
			} catch(e){}

			//Get list of categories from Google xml
			try {
				category = cur["g:google_product_category"][0];
				if(googleCategories.indexOf(category) == -1) {
					googleCategories.push(category);
				}

			} catch(e){}

			//Get product data and format into molting friendly objects

			googleProducts.push(cur);

			//googleItems[i]["g:google_product_category"][0]
			//"Home & Garden > Kitchen & Dining"

		}

		//runSyncProcessCall();
		getDbRows();
	}

	function getDbRows() {

		function terminateDbConnection() {
			dbConnection.end();
			syncLog("DB connection terminated");
		}

		syncLog("Going to get DB data");

		dbConnection = mysql.createConnection({
			host     : 'localhost',
		    user     : 'edited',
		    password : 'wJm604006!!',
		    database : 'edited_webstore'
		});

		dbConnection.connect(function(err) {
			if (err) {
				syncLog("DB connection error");
				terminateDbConnection();
			}
			else {
				syncLog("Successfully connected to DB, running query");

				dbConnection.query('SELECT id, inventory_avail FROM `xlsws_product` WHERE current = 1', function(err, rows, fields) {
					if (err) {
						syncLog("DB query error: " + err);
						terminateDbConnection();
						runSyncProcessCall();
					} else {
						dbRows = rows;
						terminateDbConnection();
						runSyncProcessCall();
						syncLog("DB queried successfully, connection terminated");
					}
				});
			}
		});

		
	}

	var calls = [processMoltinBrandData, processMoltinCategoryData, processMoltinTaxData, processMoltinProductData];

	//var calls = [processMoltinCategoryData, processMoltinTaxData];
	//var calls = [processMoltinProductData];

	function runSyncProcessCall() {

		//Pop off call and run
		if(calls.length !== 0) {
			nextCall = calls.shift();

			//Mabye catch all error here;
			nextCall();
		} else {
			finishSync();
		}


	}


	function processMoltinBrandData() {
		syncLog("Getting and processing Moltin brand data");

		function getBrandData() {

			makeMoltinRequest("https://api.molt.in/v1/brands?limit=100", "GET", null, 1, processBrandData, handleSyncError, false)

		}

		function processBrandData(brandData) {

			moltinBrands = brandData;
			//Add one to googleBrands because of extra "Unbranded" option
			/*if(googleBrands.length + 1 !== moltinBrands.length && googleBrands.length + 1 !== moltinBrands.length) {
				createBrandDataUpdates();
			} else {
				finishBrandDataSync();
			}*/

			finishBrandDataSync();

		}

		function createBrandDataUpdates() {
			//After walking through, identify which brands are needed to update and  create and run through the calls
			//If uneven number of brands create update/delete calls to keep the two in sync
			var addBrands = [],
			moltinFlatBrands = [];

			for(var i = 0; i < moltinBrands.length; i++) {
				moltinFlatBrands.push(moltinBrands[i].title);
			}

			for(var i = 0; i < googleBrands.length; i++) {
				if(moltinFlatBrands.indexOf(googleBrands[i]) == -1) addBrands.push(googleBrands[i]);
			}

			syncLog("Brands out of sync, " + addBrands.length + " brands to be added/updated");

			updateObjects = [];

			for(var i = 0; i < addBrands.length; i++) {

				var brandTitle = addBrands[i];

				var newUpdateObject = {
					"title": brandTitle,
					"slug": brandTitle.split(" ").join("-"),
					"description": brandTitle,
					"status": 1
				};

				updateObjects.push(newUpdateObject);

			}

			runBrandUpdates();

		}

		function runBrandUpdates() {

			if(updateObjects.length !== 0) {
				syncLog("Updating brand, " + updateObjects.length + " to go");

				var currentUpdateObject = updateObjects.pop();

				makeMoltinRequest("https://api.molt.in/v1/brands", "POST", currentUpdateObject, 1, runBrandUpdates, handleSyncError, false);
			} else {
				syncLog("Brand update complete, rerunning brand update check");
				getBrandData();

			}


		}

		function finishBrandDataSync() {
			syncLog("Brand sync finished");

			runSyncProcessCall();
		}

		getBrandData();
	}

	function processMoltinCategoryData() {
		syncLog("Getting and processing Moltin category data");

		function getCategoryData() {

			makeMoltinRequest("https://api.molt.in/v1/categories?limit=100", "GET", null, 1, processCategoryData, handleSyncError, false)

		}

		function processCategoryData(categoryData) {

			moltinCategories = categoryData;

			finishCategoryDataSync();

			/*if (googleCategories.length !== moltinCategories.length) {
				createCategoryDataUpdates();
			} else {
				syncLog("Categories in sync");
				//processMoltinProductData();
			}*/

		}

		function finishCategoryDataSync() {
			syncLog("Category sync finished");

			runSyncProcessCall();
		}

		getCategoryData();
	}

	function processMoltinTaxData() {
		syncLog("Getting and processing Moltin tax data");

		function getTaxData() {

			makeMoltinRequest("https://api.molt.in/v1/taxes", "GET", null, 1, processTaxData, handleSyncError, false)

		}

		function processTaxData(taxData) {

			moltinTaxes = taxData;

			finishTaxDataSync();

		}

		function finishTaxDataSync() {
			syncLog("Tax sync finished");

			runSyncProcessCall();
		}

		getTaxData();

	}

	function processMoltinProductData() {
		syncLog("Getting and processing Moltin product data");

		var googleProductJson = {},
		ignoredProducts = [],
		defaultBrandId = null,
		defaultCategoryId = null;

		function getProductData() {

			//DEBUG CHANGE TRUE AT END TO FALSE
			makeMoltinRequest("https://api.molt.in/v1/products?limit=100", "GET", null, 1, processGoogleProductData, handleSyncError, false)

		}

		function processGoogleProductData(productData) {

			moltinProducts = productData;

			syncLog("Processing Google Product Data");

			function createSlug(nonSlug) {
				var baseSlug = nonSlug.toLowerCase().replaceAll("-", "").split(" ").filter(function(n){ return n != "" }).join("-");
				var sanSlug = baseSlug.replaceAll("\\.", "").replaceAll("/-", "").replaceAll("/", "-").replaceAll("!", "").replaceAll("&", "and").replaceAll("'", "").replaceAll("(", "").replaceAll(")", "").replaceAll(",","");
				return sanSlug;
			}

			function isInStock(stockString) {
				return (stockString == "in stock") ? true : false;
			}

			function findBrandId(brandName) {
				for(var i = 0; i < moltinBrands.length; i++) {
					if(moltinBrands[i].title == brandName) return moltinBrands[i].id;
				}

				syncLog("No brand found in Moltin for: " + brandName);
				return findDefaultBrandId();
			}

			function findDefaultBrandId() {
				if(defaultBrandId == null) {
					for(var i = 0; i < moltinBrands.length; i++) {
						if(moltinBrands[i].title == "Unbranded") {
							defaultBrandId = moltinBrands[i].id;
							return defaultBrandId;
						}
					}
				} else {
					return defaultBrandId;
				}
			}

			function findCategoryId(categoryName) {

				var categoryArray = categoryName.split("&gt;");

				if(categoryArray.length == 1) {

					//This is a singular category

					var category = categoryArray[0].trim();
					for(var i = 0; i < moltinCategories.length; i++) {
						if(moltinCategories[i].title == category) return moltinCategories[i].id;
					}
				} else if(categoryArray.length == 2) {

					//This is a subcategory of a parent

					var parentCategory = categoryArray[0].trim();
					var category = categoryArray[1].trim();

					for(var i = 0; i < moltinCategories.length; i++) {
						if(moltinCategories[i].parent !== null && moltinCategories[i].parent.data.title == parentCategory) {
							if(moltinCategories[i].title == category) return moltinCategories[i].id;
						}
					}

				}


				var category = categoryName.split("&gt;")[categoryName.split("&gt;").length - 1].trim();

				syncLog("No category found in Moltin for: " + category);
				return findDefaultCategoryId();
			}

			function findDefaultCategoryId() {
				if(defaultCategoryId == null) {
					for(var i = 0; i < moltinCategories.length; i++) {
						if(moltinCategories[i].title == "Uncategorized") {
							defaultCategoryId = moltinCategories[i].id;
							return defaultCategoryId;
						}
					}
				} else {
					return defaultCategoryId;
				}

			}

			function findTaxId() {
				return moltinTaxes[0]["id"];
			}

			//Query db data here
			function findProductQuantity(id, inStock) {
				//return inStock ? 50 : 0;

				var finished = false, 
				i = 0,
				quantity = 0;

				while(!finished) {

					if(dbRows[i] !== undefined && dbRows[i].id == id) {

						var inventory_total = dbRows[i].inventory_avail;

						if(inventory_total < 0) quantity = 0;
						else quantity = inventory_total;

						finished = true;

					} else if(i == dbRows.length - 1){
						finished = true;
					} else {
						i++;
					}

				}

				return quantity;
			}

			var allIds = [], allTitles = []
			var sameIds = [], sameTitles = [];

			for(var i = 0; i < googleProducts.length; i ++) {

				try {

					//console.log("Processed google prod: " + i + "/" + googleProducts.length);

					var addThisProduct = true;

					var cur = googleProducts[i],
					id = parseInt(cur["g:id"][0]),
					title = cur["title"][0];

					if(allIds.indexOf(id) !== -1) sameIds.push(id);
					if(allTitles.indexOf(title) !== -1) sameTitles.push(title);

					allIds.push(id);
					allTitles.push(title);

					var category = ("g:product_type" in cur) ? findCategoryId(cur["g:product_type"][0]) : findDefaultCategoryId();
					var brand = ("g:brand" in cur) ? findBrandId(cur["g:brand"][0]) : findDefaultBrandId();
					var price = ("g:price" in cur) ? parseFloat(parseFloat(cur["g:price"][0]).toFixed(2)) : 0.00;
					var inStock = ("g:availability" in cur) ? (isInStock(cur["g:availability"][0])) : false;
					var description = ("description" in cur) ? cur["description"][0] : " ";

					var dataJson = {
						mainImage: null,
						backImage: null,
						allImages: [],
						gtin: "0",
						googleId: "0"
					};

					function replaceHttp(nonHttpUrl) {
						return nonHttpUrl.replace("http", "https");
					}

					if(cur["g:image_link"] !== undefined && cur["g:image_link"][0] !== undefined) {
						dataJson.mainImage = replaceHttp(cur["g:image_link"][0]);
						dataJson.allImages.push(replaceHttp(cur["g:image_link"][0]));
					}

					if(cur["g:additional_image_link"] !== undefined) {
						var finished = false,
						index = 0;

						while(!finished) {
							if(cur["g:additional_image_link"][index] !== undefined) {

								if(index == 0) dataJson.backImage = replaceHttp(cur["g:additional_image_link"][index]);
								dataJson.allImages.push(replaceHttp(cur["g:additional_image_link"][index]));
								index++;

							} else {
								finished = true;
							}
						}
					}

					if(cur["g:gtin"] !== undefined && cur["g:gtin"][0] !== '') {
						dataJson.gtin = cur["g:gtin"][0];
					}

					dataJson.googleId = id.toString();

					var jsonDescription = JSON.stringify(dataJson) + "%endjson%" + description;
 
					//Dont add products with no images
					if(dataJson.mainImage == null || dataJson.mainImage.indexOf("no_product.png") !== -1) addThisProduct = false; 

					var googleJson = {
						"status": 1,
						"sku": id,
						"title": title,
						"slug": createSlug(title),
						"price": price,
						"category": category,
						"stock_level":  findProductQuantity(id, inStock),
						"stock_status": inStock ? 1 : 3,
						"description": jsonDescription,
						"requires_shipping": 1,
						"catalog_only": 0,
						"tax_band": findTaxId(),
						"brand": brand
					};

					if(addThisProduct) {
						//Default stock level set to 50
						googleProductJson[id] = googleJson;
					} else {
						ignoredProducts.push(googleJson);
					}

				} catch(e) {
					googleProductParseFailures.push(cur);
				}

			}

			syncLog("Google Product Processing Complete, " + googleProductParseFailures.length + " failures in parsing");

			createProductDataUpdates();

		}

		function createProductDataUpdates() {

			productUpdateArray = [],
			updateObjects = [],
			productDeleteArray = [];
			//GO through the moltin data, check against each id for matching values and check the values against each other
			//First check products that need to be updated

			//NEEDS TO BE FIXED 
			for(var i = 0; i < moltinProducts.length; i++) {
				var cur = moltinProducts[i],
				id = parseInt(cur["sku"]),
				moltinId = cur["id"],
				changedValues = {},
				oldValues = {},
				valsChanged = false;

				var inStock = (cur.stock_status.data.key == "1") ? true : false;
				//Format moltin data into one that is comparable with the google data
				//Mark a property as ignore if you dont want its value to be checked

				var item_price = cur.price.data.rounded.without_tax;
				
				//Check that item is not in the sale (due to sale hack, the sale price is now the primary item price)
				var sale_item = false;
				if(cur.collection) {
					sale_item = (cur.collection.data.slug.indexOf("sale-item") !== -1);
					if(sale_item) item_price = "ignore";
				}

				var formattedCur = {
					"status": parseInt(cur.status.data.key),
					"sku": parseInt(cur.sku),
					"title": cur.title,
					"slug": cur.slug,
					"price": item_price,
					"category": Object.keys(cur.category.data)[0],
					"stock_level": cur.stock_level,
					"stock_status": (inStock) ? 1 : 3,
					"description": cur.description,
					"requires_shipping": 1,
					"catalog_only": 0,
					"tax_band": cur.tax_band.data.id,
					"brand": cur.brand.data.id
				};

				if(id in googleProductJson) {
					for(var key in googleProductJson[id]) {

						if(formattedCur[key] !== "ignore") {
							if(googleProductJson[id][key] !== formattedCur[key]) {
								valsChanged = true;
								changedValues[key] = googleProductJson[id][key];
								oldValues[key] = formattedCur[key];
							}
						}

					}

					if(valsChanged) {
						productUpdateArray.push({
							url: "https://api.molt.in/v1/products/" + moltinId,
							data: changedValues
						})
						syncLog("Updating values below for product: " + formattedCur.title);
						syncLog("FROM: " + JSON.stringify(oldValues) + "  TO: " + JSON.stringify(changedValues));
					}


					delete googleProductJson[id];
				} else {
					productDeleteArray.push(moltinId);
				}
			}

			/*console.log("--PRODUCTS TO DELETE--");
			console.log(productDeleteArray.length + " to go");

			for(var x = 0; x < productDeleteArray.length; x++) {
				console.log(productDeleteArray[x]);
			}*/

			//Now create updates for the remainder of the Google fields
			for(var key in googleProductJson) {
				updateObjects.push(googleProductJson[key]);
			}

			syncLog("Data comparison complete, " + productUpdateArray.length + " products with values to update, " + updateObjects.length + " new products to create");

			if(productUpdateArray.length == 0 && updateObjects.length == 0 && productDeleteArray.length == 0) {
				finishProductDataSync();
			} else {
				runProductUpdates();
				//runProductCreates();
			}

		}

		function runProductUpdates() {

			if(productUpdateArray.length !== 0) {
				syncLog("Updating product information, " + productUpdateArray.length + " to go");

				var currentUpdateObject = productUpdateArray.pop();

				//Debug, dont do too many
				//productUpdateArray = []

				makeMoltinRequest(currentUpdateObject.url, "PUT", currentUpdateObject.data, 1, runProductUpdates, handleSyncError);
			} else {
				syncLog("Product updates complete, moving to creates (if there are any)");
				runProductCreates();
			}


		}

		function runProductCreates() {

			if(updateObjects.length !== 0) {
				syncLog("Creating new product, " + updateObjects.length + " to go");

				var currentUpdateObject = updateObjects.pop();

				//Debug only do one
				//updateObjects = [];

				makeMoltinRequest("https://api.molt.in/v1/products", "POST", currentUpdateObject, 1, runProductCreates, handleSyncError);
			} else {
				syncLog("Product creations complete");
				//getProductData();
				runProductDeletes();
			}

		}

		function runProductDeletes() {
			if(productDeleteArray.length !== 0) {
				syncLog("Deleting products, " + productDeleteArray.length + " to go");

				var deleteProductId = productDeleteArray.pop();

				makeMoltinRequest("https://api.molt.in/v1/products/" + deleteProductId, "DELETE", null, 1, runProductDeletes, handleSyncError);
			} else {
				syncLog("Product creations complete");
				//getProductData();
				finishProductDataSync();
			}

		}

		function finishProductDataSync() {
			syncLog("Product sync finished");

			runSyncProcessCall();
		}

		getProductData();
	}




	function handleSyncError(error) {
		syncLog("Error in sync, details to follow");
		syncLog(error);

	}

	function finishSync() {
		syncLog("SYNC COMPLETE");
	}

	getGoogleXML();


}

//Run sync on startup
runSync();

//Schedule job to run every hour on the first minute 
var rule = new schedule.RecurrenceRule();
rule.minute = 1;

var j = schedule.scheduleJob(rule, runSync);




app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

//refreshAccessToken();

//addCustomer();
app.all('/', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	next();
});


app.listen(process.env.PORT || 4731);