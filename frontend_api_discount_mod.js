var express = require('express');
var app = express();

app.use(express.bodyParser());

var request = require('request');
var http = require('http');
var https = require('https');
var fs = require('fs');
var MailChimpAPI = require('mailchimp-api');
var mchimp = new MailChimpAPI.Mailchimp('e8ef30f419bf7ff29174423bf2828d14-us2');

var serverOptions = {
	key  : fs.readFileSync('keys/private_key.pem'),
	cert : fs.readFileSync('keys/certificate.pem'),
	ca   : fs.readFileSync('keys/ca_crt.pem'),
};

var nodemailer = require('nodemailer');

//My files
var email_generator = require('./email_generator');

var niftySmtpConfig = {
	host: "smtp.edited.co.uk",
	auth: {
		user: "mailer@edited.co.uk",
		pass: "wJm604006!!"
	},
	tls: {
		rejectUnauthorized: false
	} 
}

//receives emails etc.
var editedReceiveEmail = "fangaas@gmail.com;hello@edited.co.uk";
//var editedReceiveEmail = "fangaas@gmail.com";

var editedSendEmail = 'Edited Webstore <no.reply@edited.co.uk>';

function getQueryParameters(str) {
	return str.replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
}

var access_token = null;

function readAccessToken() {
	fs.readFile('access_token.txt', 'utf8', function (err,data) {
		if (err) {
			log("Access token file doesn't exist");
			return;
		}
		log("Access token loaded");
		//console.log(data);
		access_token = data;
	});
}

readAccessToken();


function writeAccessToken(token) {
	fs.writeFile('access_token.txt', token, function (err) {
		if (err) {
			log("Couldn't write to access token file");
			return; 
		}
		log("Access token updated");
	});
}

console.log("API Started, waiting for calls...");

function log(message) {
	//console.log("--" + message + "--");

	var logDate = new Date();
	//var localMessage = logDate.toLocaleTimeString() + "---" + message + "---";
	var message = logDate.toLocaleDateString() + " " + logDate.toLocaleTimeString() + "- " + message;

	console.log(message);
}

//Should not need to recall this since we have the access token
function getAccessToken(requestCallbackObject, responseObject, attempts, successCallback, failureCallback) {
	log("Getting new access token");

	request({
	    url: 'https://api.molt.in/oauth/access_token', //URL to hit
	    method: 'POST', //Specify the method
	    form: { //We can define headers too
	        'grant_type': 'client_credentials',
	        'client_id': '3fTqEGayHJKh2Gu0fNCypkE2DABja3A1WRa610o8',
	        'client_secret': 'Ca66goOpl8BjvqJ71MPHYSdpFvReYQiGz7GyHsp8'
	    }  
	}, function(error, response, body){
	    if(error) {
	        console.log(error);
	    } else {
	        access_token = JSON.parse(body).access_token;
	        writeAccessToken(access_token);
	        requestCallbackObject.auth.bearer = access_token;
	        retryRequest(requestCallbackObject, responseObject, attempts, successCallback, failureCallback);
	    }
	});
}

function refreshAccessToken(requestCallbackObject, responseObject, attempts, successCallback, failureCallback) {
	log("Refreshing access token");

	request({
	    url: 'https://api.molt.in/oauth/access_token', //URL to hit
	    method: 'POST', //Specify the method
	    form: { //We can define headers too
	        'grant_type': 'refresh_token',
	        'client_id': '3fTqEGayHJKh2Gu0fNCypkE2DABja3A1WRa610o8',
	        'client_secret': 'Ca66goOpl8BjvqJ71MPHYSdpFvReYQiGz7GyHsp8',
	        'refresh_token': access_token
	    }  
	}, function(error, response, body){
	    var bodyResp = JSON.parse(body);
	    if(bodyResp.error) {
	    	log("Could not refresh access token");
			getAccessToken(requestCallbackObject, responseObject, attempts, successCallback, failureCallback);
	    } else {
	        retryRequest(requestCallbackObject, responseObject, attempts, successCallback, failureCallback);
	    }
	});	
}

function makeRequest(url, postorget, dataObject, responseObject, attempts, successCallback, failureCallback) {
	log("Making request, URL: " + url);

	var requestObject = {
	    url: url,
	    method: postorget, //Specify the method
	    auth: {
	    	'bearer': access_token
	    }  
	};

	//Changed
	if(dataObject !== null && dataObject !== undefined) requestObject["form"] = dataObject;

	request(requestObject, function(error, response, body){
		var intFixBody = body.replace(/:(\d+)([,\}])/g, ':"$1"$2');
		var bodyResp = JSON.parse(intFixBody);

		//if(bodyResp.error == "Unable to validate access token") {
		if(bodyResp.error == "Unable to validate access token") {
			log("Current access token possibly invalid");
			refreshAccessToken(requestObject, responseObject, attempts, successCallback, failureCallback);
		} else if(bodyResp.status == false && attempts > 1) {
			attempts--;
	        retryRequest(requestObject, responseObject, attempts, successCallback, failureCallback);
		} else if(bodyResp.status == false) {
			failureCallback(responseObject, bodyResp);
		} else {
	    	successCallback(responseObject, bodyResp);
		}

	});
}

function retryRequest(requestCallbackObject, responseObject, attempts, successCallback, failureCallback) {
	log("Retrying request, remaining attempts: " + attempts);

	request(requestCallbackObject, function(error, response, body){
		var intFixBody = body.replace(/:(\d+)([,\}])/g, ':"$1"$2');
		var bodyResp = JSON.parse(intFixBody);

		if(bodyResp.error == "Unable to validate access token") {
			/*if(attempts < 3) {
				getAccessToken(requestCallbackObject, responseObject, attempts, successCallback, failureCallback);
			} else {
				failureCallback(responseObject, bodyResp);
			}*/
			getAccessToken(requestCallbackObject, responseObject, attempts, successCallback, failureCallback);
		} else if(bodyResp.status == false && attempts > 1) {
			attempts--;
			setTimeout(function() {
	        	retryRequest(requestCallbackObject, responseObject, attempts, successCallback, failureCallback);
			}, 1200);
		} else if(bodyResp.status == false) {
			failureCallback(responseObject, bodyResp);
		} else {
	    	successCallback(responseObject, bodyResp);
		}

	});

}

function defaultSuccessCallback(responseObject, body) {

	log("Success");
	//console.log(body);

	if(body == undefined) body = {
		success: true
	};

	responseObject.type('application/json');
	responseObject.send(JSON.stringify(body));

	log("Request over");

}

function defaultFailureCallback(responseObject, body) {

	log("Failure");
	//console.log(body);

	if(body == undefined) body = {
		success: false
	};

	responseObject.type('application/json');
	responseObject.send(JSON.stringify(body));

	log("Request over");

}

function blankFailureCallback() {}

function generatePassword() {

    var length = 8,
        charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".split(""),
		uppercaseSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split(""),
		lowercaseSet = "abcdefghijklnopqrstuvwxyz".split(""),
		numberSet = "0123456789".split(""),
        n = charset.length;

	var password = [];

    for (var i = 0; i < length; i++) {
        password.push(charset[Math.floor(Math.random() * n)]);
    }

    function checkInArray(array1, array2) {
	    array1.filter(function(n) {
		    return array2.indexOf(n) != -1;
		});
    }

    //Check and make sure there is an uppercase in there
    if(!checkInArray(password, uppercaseSet)) {
    	password[Math.floor(Math.random() * length)] = uppercaseSet[Math.floor(Math.random() * uppercaseSet.length)];
    }

    //Check and make sure there is a lowercase in there
    if(!checkInArray(password, lowercaseSet)) {
    	password[Math.floor(Math.random() * length)] = lowercaseSet[Math.floor(Math.random() * lowercaseSet.length)];
    }

    //Check and make sure there is a number in there
	if(!checkInArray(password, numberSet)) {
    	password[Math.floor(Math.random() * length)] = numberSet[Math.floor(Math.random() * numberSet.length)];
    }    

    //return password;
    return password.join("");

}

//For mailchimp signup at same time
function mailChimpSignup(chimpMail) {
	log("Mailchimp signup: " + chimpMail);

	mchimp.lists.subscribe({id: '5f3d5640ce', email: { email: chimpMail } }, function(data) {
		log("Mailchimp signup success");
	}, function(error) {
		log("Mailchimp signup error: " + error.error);
	});
}

function addCustomer(responseObject, customerData) {

	/*var customerData = { 
    	first_name: "Mat",
    	last_name: "Jenkins",
    	email: "fangaas@gmail.com",
    	password: "Pass",
    	signupNewsletter: true
    }*/


    function addCustomerEmailCallback(responseObject, bodyResp) {

    	//Account has been created so return success, email will now be sent as a separate process and wont affect account creation if it fails
    	defaultSuccessCallback(responseObject, bodyResp);

    	/*var emailBody = generateCustomerEmail(customerData.first_name, false, "");

    	var emailData = {
    		subject: "New account created!",
    		sendTo: customerData.email,
    		body: emailBody
    	};

    	deployEmail(emailData);*/

    	var emailSpec = {
			type: "newCustomer",
			firstName: customerData.first_name,
			email: customerData.email
		}

		var emailGen = email_generator.generateAndSendMail(emailSpec);

    	if(customerData.signupNewsletter) {
    		mailChimpSignup(customerData.email);
    	};

    }

    var url = 'https://api.molt.in/v1/customers';

	makeRequest(url, 'POST', customerData, responseObject, 1, addCustomerEmailCallback, defaultFailureCallback);

}

//EMAIL GENERATION
function generateCustomerEmail(firstName, usingGenPass, genPass) {
	var emailBody = "Thank you " + firstName + " for signing up to the Edited web store, your life will be infinitely better because of this.";

	if(usingGenPass) {
		emailBody += "<br>Your generated password is: <b>" + genPass + "</b><br> To change the password, please log in and goto the change password option under account management";
	}

	return emailBody;
}

function generatePasswordResetEmail(firstName, genPass) {

	var emailBody = "Hi " + firstName + ", your account password has been reset<br>Use password <b>" + genPass + "</b> to log back in and change the password to something more memorable under account management";

	return emailBody;

}

//EMAIL GENERATION
function editedWrapEmailBody(body) {
	return body;
}


function loginCustomer(responseObject, loginData) {

	/*var loginData = {
		email: "fangaas@gmail.com",
		password: "88Zfmcew"
	}*/

	//var url = 'https://api.molt.in/v1/customers/authenticate';

	var url = 'https://api.molt.in/v1/customers/token';

	makeRequest(url, 'POST', loginData, responseObject, 1, defaultSuccessCallback, defaultFailureCallback);

}


function logoutCustomer(responseObject, logoutData) {

	/*var logoutData = {
		token: "xxx"
	};*/

	var url = 'https://api.molt.in/v1/customers/token/' + logoutData.token;

	makeRequest(url, 'DELETE', null, responseObject, 1, defaultSuccessCallback, defaultFailureCallback);

}
 
function findCustomerByField(responseObject, customerData) {

	//Currently not working but necessary for some features

	var customerData = {
		email: "fangaas@gmail.com"
	};

	var url = "https://api.molt.in/v1/customers/search";

	makeRequest(url, 'GET', customerData, responseObject, 1, defaultSuccessCallback, defaultFailureCallback);

}

function getAddressesForCustomer(responseObject, customerData) {

	/*var customerData = {
		id: "1212291565293142332",
		token: "xxx"
	}*/

	var url = "https://api.molt.in/v1/customers/" + customerData.token + "/addresses";

	makeRequest(url, 'GET', null, responseObject, 1, defaultSuccessCallback, defaultFailureCallback);

}

function deleteAddressForCustomer(responseObject, addressData) {

	/*var addressData = {
		addressId: xxx,
		customerId: xxx
	}*/

	var url = "https://api.molt.in/v1/customers/" + addressData.customerId + "/addresses/" + addressData.addressId;

	makeRequest(url, 'DELETE', null, responseObject, 1, defaultSuccessCallback, defaultFailureCallback);

}

function changeCustomerPassword(responseObject, customerData) {

	/*var customerData = {
        id: customerId,
        email: email,
        pass: newPass
    }*/

    function changeCustomerPasswordCallback(responseObject, bodyResp) {

    	defaultSuccessCallback(responseObject, bodyResp);

    	/*var emailBody = "Your password has been changed against your Edited account, if this wasn't you then please contact support@edited.co.uk and we can help you out.";

    	var emailData = {
    		subject: "Password changed",
    		sendTo: customerData.email,
    		body: emailBody
    	};

    	deployEmail(emailData);*/

    	var emailSpec = {
			type: "changePassword",
			email: customerData.email,
			firstName: customerData.first_name
    	}

		var emailGen = email_generator.generateAndSendMail(emailSpec);

    }

    var url = "https://api.molt.in/v1/customers/" + customerData.id;

	var changeData = {
		password: customerData.pass
	};

	makeRequest(url, 'PUT', changeData, responseObject, 1, changeCustomerPasswordCallback, defaultFailureCallback);

}

function resetCustomerPassword(responseObject, customerData) {

	/* var customerData = {
		email: email	
	}*/

	function foundCustomerCallback(responseObject, customer) {

		var customerId = customer.id,
		firstName = customer.first_name,
		email = customer.email;

		newPass = generatePassword();

		log("Using pass: " + newPass);

		function resetCustomerPasswordCallback(responseObject, body) {

    		defaultSuccessCallback(responseObject, body);

			sendPasswordResetEmail(firstName, newPass, email);

		}

		var url = "https://api.molt.in/v1/customers/" + customerId;

		var changeData = {
			password: newPass
		};

		makeRequest(url, 'PUT', changeData, responseObject, 1, resetCustomerPasswordCallback, defaultFailureCallback);

	}

	var filter = {
		type: "email",
		val: customerData.email
	};

	findCustomerByFilter(responseObject, filter, foundCustomerCallback);

}

/*resetCustomerPassword(null, {
	email: "fangaas@gmail.com"
});*/

//Internal function
function findCustomerByFilter(responseObject, filter, successCallback, failureCallback) {

	if(successCallback == undefined) successCallback = defaultSuccessCallback;
	if(failureCallback == undefined) failureCallback = defaultFailureCallback;

	/*var filter = {
		type: "email",
		val: "fangaas@gmail.com"
	}*/

	//CUSTOMER FILTERS CURRENTLY NOT WORKING, SO DOING IT MANUALLY
	function getCustomersCallback(responseObject, customers) {

		var allCustomers = customers.result, 
		matchCustomer = null;

		//Customers array
		if(filter.type == "email") {

			for(var i = 0; i < allCustomers.length; i ++) {
				if(allCustomers[i].email == filter.val) {
					matchCustomer = allCustomers[i];
					break;
				}
			}

		}

		if(matchCustomer == null) {
			failureCallback(responseObject);
		} else {
			successCallback(responseObject, matchCustomer);
		}

	}

	var url = "https://api.molt.in/v1/customers?limit=100"; 

	makeRequest(url, 'GET', null, responseObject, 1, getCustomersCallback, failureCallback);	

}

function getOrderSummary(responseObject, orderData, successCallback, failureCallback) {

	if(successCallback == undefined) successCallback = defaultSuccessCallback;
	if(failureCallback == undefined) failureCallback = defaultFailureCallback;

	/*var orderData = {
		orderId: "1215906060838109391"
	};*/

	var url = 'https://api.molt.in/v1/orders/' + orderData.orderId;

	makeRequest(url, 'GET', null, responseObject, 10, successCallback, failureCallback);	

}

function getOrderItems(responseObject, orderData, successCallback, failureCallback) {

	if(successCallback == undefined) successCallback = defaultSuccessCallback;
	if(failureCallback == undefined) failureCallback = defaultFailureCallback;

	/*var orderData = {
		orderId: "1215906060838109391"
	};*/

	var url = 'https://api.molt.in/v1/orders/' + orderData.orderId + "/items";

	makeRequest(url, 'GET', null, responseObject, 1, successCallback, failureCallback);		

}

function getFilteredOrders(responseObject, filterData) {

	/*var filterData = {
		customer: '1244289543155220845'
	}*/

	//var url = 'https://api.molt.in/v1/orders/search';

	//makeRequest(url, 'GET', filterData, responseObject, 1, successCallback, defaultFailureCallback);

	var returnBody = {
		success: true
	};


	responseObject.type('application/json');
	responseObject.send(JSON.stringify(returnBody));



	function successCallback(responseObject, data) {

		responseObject.type('application/json');
		responseObject.send(JSON.stringify(data.result));

	}

}


function authoriseCard(responseObject, userJson) {

	/*var userJson = {
        cardData: cardData
    };*/

    var cardData = userJson.cardData;

    var url = "https://api.molt.in/v1/checkout/payment/authorize";

    makeRequest(url, 'POST', cardData, responseObject, 3, successCallback, defaultFailureCallback);

    function successCallback(responseObject, data) {

		log("Card verified successfully!");

		responseObject.type('application/json');
		responseObject.send(JSON.stringify(data));

		//These can run in parallel no worries
		changeOrderAccount(data, accountCreationData);
		deployOrderEmails(data);

	}


}

/*var userJson = {
	"cardData": {
		"data": {
	        "first_name": "John",
	        "last_name": "Doe",
	        "number": "4242424242424242",
	        "expiry_month": "08",
	        "expiry_year": "2020",
	        "cvv": "123"
	    }	
	}
};

authoriseCard(null, userJson);*/


function completeOrderOnsite(responseObject, userJson) {

	log("Beggining onsite order processing");

	/*
	var userJson = {
			orderId: orderId,
			cardData: cardData,
            accountCreationData: accountCreationData
		};
	*/

	var orderId = userJson.orderId,
	cardData = userJson.cardData,
	accountCreationData = userJson.accountCreationData;

	var url = 'https://api.molt.in/v1/checkout/payment/purchase/' + orderId;

	//Try this one 3 times because its very very important
	makeRequest(url, 'POST', cardData, responseObject, 3, successCallback, defaultFailureCallback);

	function successCallback(responseObject, data) {

		log("Payment completed!");

		responseObject.type('application/json');
		responseObject.send(JSON.stringify(data));

		//These can run in parallel no worries
		changeOrderAccount(data, accountCreationData);
		deployOrderEmails(data);

	}

}

function completeOrderPaypal(responseObject, userJson) {

	log("Beggining paypal order processing");

	/*var userJson = {
        amount: "30",
        paypalToken: "EC-9FK65878NC966722E",
        paypalPayerId: "6FU5UTD4MP9AJ",
        orderId: "1328251121323475607",
        accountCreationData: {createAccount: false, customPassword: "undefined", signupNewsletter: "false"} 
    };*/

	var orderId = userJson.orderId,
	accountCreationData = userJson.accountCreationData;


	function paypalCompletedCallback() {

		var url = "https://api.molt.in/v1/orders/" + orderId;

		var data = {
			status: "paid"
		};

		makeRequest(url, 'PUT', data, responseObject, 3, moltinSuccessCallback, defaultFailureCallback);

		function moltinSuccessCallback(responseObject, data) {

			log("Moltin after paypal success, deploying order emails and account updates");

			responseObject.type('application/json');
			responseObject.send(JSON.stringify(data));

			//These can run in parallel no worries
			changeOrderAccount(data, accountCreationData);
			deployOrderEmails(data);

		}

	}

	paypalDoExpressCheckoutPayment(responseObject, userJson, paypalCompletedCallback);

}


function changeOrderAccount(orderBody, accountCreationData) {

	/*var accountCreationData = {
    	createAccount: true,
    	useCustomPassword: true,
    	customPassword: xxx
    };*/

    if(orderBody.result.order == null) orderBody.result.order = orderBody.result;

    if(accountCreationData.signupNewsletter) {
    	mailChimpSignup(orderBody.result.order.customer.data.email);
    }

	if(accountCreationData.createAccount) {

		var newPass;

		if(accountCreationData.useCustomPassword) newPass = accountCreationData.customPassword;
		else newPass = generatePassword();

		log("Using pass: " + newPass);

		//Customer ID here and update
		var customerId = orderBody.result.order.customer.data.id;

		function changeOrderAccountCallback() {

			/*var emailBody = generateCustomerEmail(orderBody.result.order.customer.data.first_name, !accountCreationData.useCustomPassword, newPass);

	    	var emailData = {
	    		subject: "New account created!",
	    		sendTo: orderBody.result.order.customer.data.email,
	    		body: emailBody
	    	};

	    	deployEmail(emailData);*/

	    	var emailSpec;

	    	if(!accountCreationData.useCustomPassword) {
	    		emailSpec = {
					type: "newCustomerGenPass",
					firstName: orderBody.result.order.customer.data.first_name,
					email: orderBody.result.order.customer.data.email,
					generatedPassword: newPass
				}
	    	} else {
	    		emailSpec = {
					type: "newCustomer",
					firstName: orderBody.result.order.customer.data.first_name,
					email: orderBody.result.order.customer.data.email
				}
	    	}

			var emailGen = email_generator.generateAndSendMail(emailSpec);

		}

		var url = "https://api.molt.in/v1/customers/" + customerId;

		var changeData = {
			password: newPass
		};

		makeRequest(url, 'PUT', changeData, null, 1, changeOrderAccountCallback, blankFailureCallback);

	}

}



function deployOrderEmails(orderBody) {


	//if(orderBody.result.order == null) orderId = orderBody.result.id;
	//else orderId = orderBody.result.order.id;

	//UNCOMMENT
	//if(orderBody.result.order == null) orderBody.result.order = orderBody.result;
	//UNCOMMENT
	//var orderId = orderBody.result.order.id;


	var receiptSummaryJson = "No data",
	receiptItemsJson = "No data";

	function getReceiptData() {

		//UNCOMMENT
		/*var orderData = {
			orderId: orderId
		};*/
		var orderData = {
			orderId: "1311935949390741832"
		};

		function receiptSummaryCallback(responseObject, data) {
			receiptSummaryJson = data.result;

			getOrderItems(null, orderData, receiptItemsCallback, receiptDataFailureCallback);
		}

		function receiptItemsCallback(responseObject, data) {
			var rawItemsJson = data.result;

			receiptItemsJson = calculateSaleAmounts(receiptItemsJson);

			deployReceiptToCustomer();
			deployDetailsToEdited();
		}

		function calculateSaleAmounts(items) {
			try {
				var baseDiscountActive = false,
				baseDiscountAmount = 0,
				receiptTotal = receiptSummaryJson.subtotal,
				itemTotal = 0;

				for(var i = 0; i < receiptItemsJson.length; i ++) {
					itemTotal += parseFloat(receiptItemsJson[i].price.data.rounded.with_tax);
				}

				if(receiptTotal != itemTotal) {
					baseDiscountActive = true;
					var receiptPercDiff = (receiptTotal / itemTotal);
					baseDiscountAmount = parseFloat(receiptPercDiff.toFixed(1)) * 100
				}

				for(var i = 0; i < receiptItemsJson.length; i++) {
					var cur = receiptItemsJson[i].product.data;

					if(cur.sale_price != undefined) {
						var originalPrice = parseFloat(cur.sale_price);
						//var salePrice = 
					}
				}


			} catch(e) {
				return items;
			}
		}

		function receiptDataFailureCallback() {

			deployReceiptToCustomer();
		} 

		getOrderSummary(null, orderData, receiptSummaryCallback, receiptDataFailureCallback);

	}

	//EMAIL GENERATION
	/*function generateReceipt() {
		return "Thanks for your order<br>" + JSON.stringify(receiptSummaryJson) + " " + JSON.stringify(receiptItemsJson);
	}

	//EMAIL GENERATION
	function generateOrderInfo() {
		return "New order<br>" + JSON.stringify(receiptSummaryJson) + " " + JSON.stringify(receiptItemsJson);
	}*/

	function modifiyItemsArray() {

		//Here we will generate the discount percentage used on each item

	}

	function deployReceiptToCustomer() {

		var customerEmail;

		//get email here
		if(orderBody.result.order == null) customerEmail = orderBody.result.customer.data.email;
		else customerEmail = orderBody.result.order.customer.data.email;

		/*var customerEmailData = {
			subject: "Order Confirmed",
			sendTo: customerEmail,
			body: generateReceipt()
		}

		deployEmail(customerEmailData, null, deployDetailsToEdited, deployDetailsToEdited);*/

		var emailSpec = {
			type: "customer",
			email: customerEmail,
			receiptSummaryJson: receiptSummaryJson,
			receiptItemsArray: receiptItemsJson
		};

		//email_generator.generateAndSendReceipt(emailSpec);

	}

	function deployDetailsToEdited() {

		/*var editedEmailData = {
			subject: "(TEST) New Web Order",
			//sendTo: "hello@edited.co.uk, fangaas@gmail.com",
			sendTo: "fangaas@gmail.com",
			body: generateOrderInfo()
		}

		deployEmail(editedEmailData, null);*/

		var emailSpec = {
			type: "internal",
			email: editedReceiveEmail,
			receiptSummaryJson: receiptSummaryJson.result,
			receiptItemsArray: receiptItemsJson.result
		};

		//email_generator.generateAndSendReceipt(emailSpec);

	}

	getReceiptData();

}

//REMOVE
//deployOrderEmails();

/*var testOrderJson = {
    "orderId": "1215906060838109391",
    "cardData": {
        "data": {
            "first_name": "John",
            "last_name": "Doe",
            "number": "4242424242424242",
            "expiry_month": "08",
            "expiry_year": "2020",
            "cvv": "123"
        }
    }
};

completeOrderOnsite(null, testOrderJson);*/

function sendPasswordResetEmail(firstName, newPass, email) {


	/*var emailData = {
		subject: "Account password reset",
		sendTo: email,
		body: emailBody
	};

	deployEmail(emailData);*/

	var emailSpec = {
		type: "passwordReset",
		firstName: firstName,
		email: email,
		generatedPassword: newPass
	}

	var emailGen = email_generator.generateAndSendMail(emailSpec);
}

//sendPasswordResetEmail("Matthew", "password", "fangaas@gmail.com");

function sendFeedbackEmail(responseObject, feedbackData) {

	/*var feedbackData = {
		message: "Message"
	}*/

	var emailSpec = {
		type: "feedbackEmail",
		message: feedbackData.message
	};

	email_generator.generateAndSendMail(emailSpec);

	defaultSuccessCallback(responseObject);

}

/*sendFeedbackEmail(null, {
	message: "Hows it going"
});*/


function deployEmail(emailData, responseObject, successCallback, errorCallback) {

	/*emailData = {
		subject: xx,
		sendTo: xx,
		body: xx,
		html: xx,
		textOnly: xx
	}*/

	var transporter = nodemailer.createTransport(niftySmtpConfig);

	var subject = emailData.subject;

	var htmlBody, textOnly;

	if(emailData.html != undefined) {
		htmlBody = emailData.htmlBody;
	} else {
		htmlBody = emailData.body;
	}

	if(emailData.textOnly != undefined) {
		textOnly = emailData.textOnly;
	} else {
		textOnly = emailData.body;
	}

	var sendTo = emailData.sendTo;

	var mailOptions = {
	    from: editedSendEmail, // sender address
	    to: sendTo, // list of receivers
	    subject: subject, // Subject line
	    text: textOnly, // plaintext body
	    html: htmlBody // html body
	};

	transporter.sendMail(mailOptions, function(error, info){

	    if(error){
	     	log("ERROR when sending message: " + error);
	     	if(errorCallback != undefined) errorCallback(responseObject);
	     	
	    } else {
	     	log("Message sent successfully");
	     	if(successCallback != undefined) successCallback(responseObject);
	    }
	    
	});

}

function paypalSetExpressCheckout(responseObject, data) {

	/*data = {
        amount: amount,
        orderId: orderId,
        accountCreationData: accountCreationData            
    };*/

    /*var data = {
    	amount: "10.00",
    	orderId: 111,
    	accountCreationData: {
	    	createAccount: false,
	    	useCustomPassword: false,
	    	customPassword: false,
	    	signupNewsletter: false
	    }
    };*/

    var returnUrl = "http://edited.co.uk/checkout?paypal=success";
    returnUrl += "&orderId=" + data.orderId;
    returnUrl += "&createAccount=" + data.accountCreationData.createAccount;
    returnUrl += "&useCustomPassword=" + data.accountCreationData.useCustomPassword;
    returnUrl += "&customPassword=" + data.accountCreationData.customPassword;
    returnUrl += "&signupNewsletter=" + data.accountCreationData.signupNewsletter;

    var cancelUrl = returnUrl.replace("success", "cancel");

	/* TEST DETAILS
	var requestObject = {
	    url: "https://api-3t.sandbox.paypal.com/nvp",
	    method: "POST", //Specify the method
	    form: {
	    	USER: "platfo_1255077030_biz_api1.gmail.com",
  			PWD: "1255077037",
  			SIGNATURE: "Abg0gYcQyxQvnf2HDJkKtA-p6pqhA1k-KTYE0Gcy1diujFio4io5Vqjf",
  			METHOD: "SetExpressCheckout",
  			VERSION: "78",
  			PAYMENTREQUEST_0_PAYMENTACTION: "SALE",
  			PAYMENTREQUEST_0_AMT: data.amount,
  			PAYMENTREQUEST_0_CURRENCYCODE: "GBP",
  			NOSHIPPING: "1",
  			//cancelUrl: "http://editedfresh.co.uk/checkout?paypal=cancel",
  			returnUrl: returnUrl,
  			cancelUrl: cancelUrl
  			//returnUrl: "http://editedfresh.co.uk/checkout?paypal=success&delivery=" + data.chosenDelivery
  			//returnUrl: "https://www.paypal.com/checkoutnow/"
	    }
	};*/

	var requestObject = {
	    url: "https://api-3t.paypal.com/nvp",
	    method: "POST", //Specify the method
	    form: {
	    	USER: "paypal_api1.edited.co.uk",
  			PWD: "544WVRL8A2RTMS3Z",
  			SIGNATURE: "AYVhf5meTS8xugQlwzHfwsxgiItyAQNX6M0yB1mEXwCVXrLuwXonT7I5",
  			METHOD: "SetExpressCheckout",
  			VERSION: "78",
  			PAYMENTREQUEST_0_PAYMENTACTION: "SALE",
  			PAYMENTREQUEST_0_AMT: data.amount,
  			PAYMENTREQUEST_0_CURRENCYCODE: "GBP",
  			NOSHIPPING: "1",
  			returnUrl: returnUrl,
  			cancelUrl: cancelUrl
	    }
	};

	request(requestObject, function(error, response, body){
		
		var bodyObj = getQueryParameters(body);

		var returnBody;

		if(bodyObj.ACK == "Success") {
			returnBody = {
				success: true,
				token: bodyObj.TOKEN 
			};
		} else {
			returnBody = {
				success: false
			};
		}


		responseObject.type('application/json');
		responseObject.send(JSON.stringify(returnBody));

	});

}

function paypalDoExpressCheckoutPayment(responseObject, data, callback) {

	/*data = {
		amount: "10.00",
        paypalToken: "EC-3WX0164794767931R",
        paypalPayerId: "ZAZMUZCRPPP5J",
        orderId: "1236941667731243693",
        accountCreationData: {}        
    };*/


  	var requestObject = {
	    //url: "https://api-3t.sandbox.paypal.com/nvp",
	    url: "https://api-3t.paypal.com/nvp",
	    method: "POST", //Specify the method
	    form: {
	    	USER: "paypal_api1.edited.co.uk",
  			PWD: "544WVRL8A2RTMS3Z",
  			SIGNATURE: "AYVhf5meTS8xugQlwzHfwsxgiItyAQNX6M0yB1mEXwCVXrLuwXonT7I5",
  			METHOD: "DoExpressCheckoutPayment",
  			VERSION: "93",
  			PAYMENTREQUEST_0_PAYMENTACTION: "SALE",
  			PAYMENTREQUEST_0_AMT: data.amount,
  			PAYMENTREQUEST_0_CURRENCYCODE: "GBP",
  			TOKEN: data.paypalToken,
  			PAYERID: data.paypalPayerId
	    }
	};

	request(requestObject, function(error, response, body){
		
		var bodyObj = getQueryParameters(body);

		log("Paypal request finished, object:");
		log(JSON.stringify(bodyObj));

		if(bodyObj.ACK == "Success") {
			callback(responseObject, data);
		} else {

			var returnBody = {
				success: false
			};

			responseObject.type('application/json');
			responseObject.send(JSON.stringify(returnBody));

		}

	});

}

function sendContactFormEmail(responseObject, data) {

	var emailSpec = data;
	emailSpec.type = "sendContactFormEmail";

	email_generator.generateAndSendMail(emailSpec);

	var returnBody = {
		success: true
	};

	responseObject.type('application/json');
	responseObject.send(JSON.stringify(returnBody));

}


app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

//refreshAccessToken();

//addCustomer();
app.all('/', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	next();
});


app.get('/', function(req, res, next) {

	res.type('text/plain');
	res.send('i am a beautiful butterfly');

});

app.post('/addCustomer', function(req, res) {

	addCustomer(res, req.body);

});

app.post('/loginCustomer', function(req, res) {

	loginCustomer(res, req.body);

});

app.post('/logoutCustomer', function(req, res) {

	logoutCustomer(res, req.body);

});

app.post('/authoriseCard', function(req, res) {

	authoriseCard(res, req.body);

});

app.post('/completeOrderOnsite', function(req, res) {

	completeOrderOnsite(res, req.body);

});

app.post('/completeOrderPaypal', function(req, res) {

	completeOrderPaypal(res, req.body);

});

app.post('/getOrderSummary', function(req, res) {

	getOrderSummary(res, req.body);

});

app.post('/getOrderItems', function(req, res) {

	getOrderItems(res, req.body);

});

app.post('/getFilteredOrders', function(req, res) {

	getFilteredOrders(res, req.body);

});

app.post('/findCustomerByField', function(req, res) {

	findCustomerByField(res, req.body);

});

app.post('/getAddressesForCustomer', function(req, res) {

	getAddressesForCustomer(res, req.body);

});

app.post('/deleteAddressForCustomer', function(req, res) {

	deleteAddressForCustomer(res, req.body);

});

app.post('/changeCustomerPassword', function(req, res) {

	changeCustomerPassword(res, req.body);

});

app.post('/resetCustomerPassword',  function(req, res) {

	resetCustomerPassword(res, req.body);

});

app.post('/feedbackEmail', function(req, res) {

	sendFeedbackEmail(res, req.body);

});

app.post("/paypalSetExpressCheckout", function(req, res) {

	paypalSetExpressCheckout(res, req.body);

});

app.post("/sendContactFormEmail", function(req, res) {

	sendContactFormEmail(res, req.body);

});

//app.listen(process.env.PORT || 4830);

https.createServer(serverOptions, app).listen(4830);

//addCustomer();